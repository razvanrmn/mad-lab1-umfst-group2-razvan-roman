import java.util.Random;
import java.util.Scanner;


public class Main {
    public static boolean is_Prime(long n) {

        if (n < 2) {
            return false;
        }

        for (int i = 2; i <= n / 2; i++) {

            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
    public static void main(String[] args) {
        // Ex1
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number: ");
        int number = sc.nextInt();

        if (number < 0) {
            System.out.println("Negative");
        }
        else {
            System.out.println("Positive");
        }

        // Ex2
        System.out.println("Enter n value: ");
        int n = sc.nextInt();
        int[] myArray = new int[n];
        int sum = 0;
        Random rand = new Random();
        for (int i = 0; i < n; ++i) {
            myArray[i] = rand.nextInt(20);
        }
        for (int i = 0; i < n; ++i) {
            sum += myArray[i];
        }
        System.out.println(sum);

        // Ex3
        for (int i = 2; i < 100; ++i) {

            if (is_Prime(i) && is_Prime(i + 2)) {
                System.out.printf("(%d, %d)\n", i, i + 2);
            }
        }

        // Ex4

    }
}