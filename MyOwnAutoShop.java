public class MyOwnAutoShop {
    public static void main(String[] args) {
        Sedan sedan = new Sedan(1,2, "green", 2);
        Ford fordA = new Ford(1,2, "green", 2);
        Ford fordB = new Ford(1,2, "green", 2);

        Car car = new Car(1,2, "green");

        System.out.println(sedan.getSalePrice());
        System.out.println(fordA.getSalePrice());
        System.out.println(fordB.getSalePrice());
        System.out.println(car.getSalePrice());
    }
}
