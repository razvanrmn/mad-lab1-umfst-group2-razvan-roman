public class Truck extends Car {
    private int weight;
    public Truck(int speed, double regularPrice, String color, int weight) {
        super(speed, regularPrice, color);
        this.weight = weight;
    }
    public double getSalePrice() {
        if (this.weight > 2000) {
            return (this.regularPrice - this.regularPrice * 10/100);
        }
        else
            return (this.regularPrice - this.regularPrice * 20/100);
    }

}
