public class Car {
    protected  int speed;
    protected double regularPrice;
    protected String color;

    public Car() {

    }

    public Car(int speed, double regularPrice, String color) {
        this.speed = speed;
        this.regularPrice = regularPrice;
        this.color = color;
    }

    public double getSalePrice() {
        if (this.speed > 2000) {
            return (this.regularPrice - this.regularPrice * 5/100);
        }
        else
            return (this.regularPrice - this.regularPrice * 6/100);
    }
}

