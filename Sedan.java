public class Sedan extends Car {
    private int length;

    public Sedan(int speed, double regularPrice, String color, int length) {
        super(speed, regularPrice, color);
        this.length = length;
    }
    public void setLength(int length) {
        this.length = length;
    }
    public int getLength() {
        return this.length;
    }
    public double getSalePrice() {
        if (this.length > 20) {
            return (this.regularPrice - this.regularPrice * 5/100);
        }
        else
            return (this.regularPrice - this.regularPrice * 10/100);
    }
}
